
var table :HTMLTableElement = <HTMLTableElement> document.getElementById("table");
function solve (): void {
    var N:HTMLInputElement = <HTMLInputElement> document.getElementById("T1");
    var num:number=parseFloat(N.value);  //To handle float numbers
    while (table.rows.length!=0)
    {
        table.deleteRow(0);
    }
    if(!isNaN(num))
    {
        var count:number = parseInt(N.value);   //To make sure the for loop will make iterations= integer part of number
    
        var p:HTMLParagraphElement = <HTMLParagraphElement> document.getElementById("p");
        p.innerHTML = "<h1>The table is:</h1>"; 
       
        for (var i:number = 1;i<=count; i=i+1)
        { 
            var row: HTMLTableRowElement = table.insertRow();
            var cell:HTMLTableCellElement=row.insertCell();
            cell.style.textAlign="center";
            cell.bgColor = "red";
            cell.innerHTML = num.toString();
            var cell:HTMLTableCellElement=row.insertCell();
            cell.style.textAlign="center";
            cell.bgColor = "grey";
            cell.innerHTML = "*";
            var cell:HTMLTableCellElement=row.insertCell();
            cell.style.textAlign="center";
            cell.bgColor = "yellow";
            cell.innerHTML =  i.toString();
            var cell:HTMLTableCellElement=row.insertCell();
            cell.style.textAlign="center";
            cell.bgColor = "gray";
            cell.innerHTML = "=";
            var cell:HTMLTableCellElement=row.insertCell();
            cell.style.textAlign="center";
            cell.bgColor = "red";
            cell.innerHTML = (num*i).toString();
        }
    }
    else
    {
        var p:HTMLParagraphElement = <HTMLParagraphElement> document.getElementById("p");
        p.innerHTML = "<h1>PLEASE ENTER A VALID NUMBER</h1>"; 
    }
}
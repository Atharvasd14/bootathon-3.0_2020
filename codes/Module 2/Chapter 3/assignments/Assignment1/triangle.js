class point {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
//Below 8 lines are used to get the input elements for point A , B , C & P:
var Ax = document.getElementById("Ax");
var Ay = document.getElementById("Ay");
var Bx = document.getElementById("Bx");
var By = document.getElementById("By");
var Cx = document.getElementById("Cx");
var Cy = document.getElementById("Cy");
var Px = document.getElementById("Px");
var Py = document.getElementById("Py");
function numcheck(P) {
    if (!isNaN(P.x) && !isNaN(P.y)) {
        return true;
    }
    return false;
}
function find() {
    //The 4 lines below are used to initialize points A,B,C and P using the constructor 
    var A = new point(parseFloat(Ax.value), parseFloat(Ay.value));
    var B = new point(parseFloat(Bx.value), parseFloat(By.value));
    var C = new point(parseFloat(Cx.value), parseFloat(Cy.value));
    var P = new point(parseFloat(Px.value), parseFloat(Py.value));
    var pt = numcheck(A) && numcheck(B) && numcheck(C) && numcheck(P); //Is all 4 pts are valid
    var res;
    if (pt == true) {
        var Ar = Area(A, B, C); //Area of triangle ABC
        if (Ar != 0) //If triangle is not a straight line
         {
            var A1 = Area(P, B, C); //Area of triangle PBC
            var A2 = Area(P, A, C); //Area of triangle PAC
            var A3 = Area(P, A, B); //Area of triangle PAB   
            var sum = A1 + A2 + A3;
            if (Ar + 1 > sum && Ar - 1 < sum) // ie A1+A2+A3 must lie between Ar+1 and Ar-1. This is done because in case of decimal nos, Ar==Sum may be wrong.
             {
                res = "YES the point P(" + P.x + "," + P.y + ") lies inside the triangle ABC";
            }
            else {
                res = "NO the point P(" + P.x + "," + P.y + ") DOESNOT lie inside the triangle ABC";
            }
        }
        else {
            res = "The triangle is colinear ";
        }
    }
    else {
        res = "Please enter valid coordinates";
    }
    var para = document.getElementById("para");
    para.innerHTML = "<h1>" + res + "</h1>";
}
function Area(A, B, C) {
    var area = 0;
    var AB = Math.sqrt(Math.pow((A.x - B.x), 2) + Math.pow((A.y - B.y), 2)); //Length of side AB of triangle ABC
    var BC = Math.sqrt(Math.pow((B.x - C.x), 2) + Math.pow((B.y - C.y), 2)); //Length of side BC of triangle ABC
    var CA = Math.sqrt(Math.pow((A.x - C.x), 2) + Math.pow((A.y - C.y), 2)); //Length of side CA of triangle ABC
    var s = (AB + BC + CA) / 2; //SemiPerimeter s
    area = Math.sqrt(s * (s - AB) * (s - BC) * (s - CA)); //Area is calculated
    return area;
}
//# sourceMappingURL=triangle.js.map
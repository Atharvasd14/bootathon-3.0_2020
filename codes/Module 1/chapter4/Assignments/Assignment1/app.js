var i1 = document.getElementById("i1");
var i2 = document.getElementById("i2"); //i1&i2 are input numbers for addition
var i4 = document.getElementById("i4");
var i5 = document.getElementById("i5"); //i4&i5 are input numbers for subtraction
var i7 = document.getElementById("i7");
var i8 = document.getElementById("i8"); //i7&i8 are input numbers for multiplication
var i10 = document.getElementById("i10");
var i11 = document.getElementById("i11"); //i10&i11 are input numbers for division
var l1 = document.getElementById("l1");
var l2 = document.getElementById("l2");
var l3 = document.getElementById("l3");
var l4 = document.getElementById("l4");
function add() {
    var a = parseFloat(i1.value);
    var b = parseFloat(i2.value);
    if (!isNaN(a) && !isNaN(b)) //If a and b both are valid numbers
     {
        var c = a + b;
        l1.innerHTML = c + "";
    }
    else {
        l1.innerHTML = "You have entered invalid inputs";
    }
}
function mul() {
    var a = parseFloat(i7.value);
    var b = parseFloat(i8.value);
    if (!isNaN(a) && !isNaN(b)) //If a and b both are valid numbers
     {
        var c = a * b;
        l3.innerHTML = c + "";
    }
    else {
        l3.innerHTML = "You have entered invalid inputs";
    }
}
function sub() {
    var a = parseFloat(i4.value);
    var b = parseFloat(i5.value);
    if (!isNaN(a) && !isNaN(b)) //If a and b both are valid numbers
     {
        var c = a - b;
        l2.innerHTML = c + "";
    }
    else {
        l2.innerHTML = "You have entered invalid inputs";
    }
}
function div() {
    var a = parseFloat(i10.value);
    var b = parseFloat(i11.value);
    if (!isNaN(a) && !isNaN(b)) //If a and b both are valid numbers
     {
        var c = a / b;
        l4.innerHTML = c + "";
    }
    else {
        l4.innerHTML = "You have entered invalid inputs";
    }
}
//# sourceMappingURL=app.js.map
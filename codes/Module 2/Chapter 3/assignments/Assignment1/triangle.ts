class point {       //poin tis a user defined variable which holds x & y coordinate
    x:number;
    y:number;
    constructor(x: number, y: number) {     //Constructor to initialize variables x&y
        this.x = x;
        this.y = y;
    }
}
//Below 8 lines are used to get the input elements for point A , B , C & P:
var Ax:HTMLInputElement = <HTMLInputElement> document.getElementById("Ax");
var Ay:HTMLInputElement = <HTMLInputElement> document.getElementById("Ay");
var Bx:HTMLInputElement = <HTMLInputElement> document.getElementById("Bx");
var By:HTMLInputElement = <HTMLInputElement> document.getElementById("By");
var Cx:HTMLInputElement = <HTMLInputElement> document.getElementById("Cx");
var Cy:HTMLInputElement = <HTMLInputElement> document.getElementById("Cy");
var Px:HTMLInputElement = <HTMLInputElement> document.getElementById("Px");
var Py:HTMLInputElement = <HTMLInputElement> document.getElementById("Py");
function numcheck(P:point):boolean  //Returns true if both tX&y of a point are valid else reurns false
{
    if (!isNaN(P.x)&&!isNaN(P.y))
    {
        return true;
    }
    return false;
}
function find() :void{      //function which is called when the button is pressed
    
    //The 4 lines below are used to initialize points A,B,C and P using the constructor 
    var A:point = new point(parseFloat(Ax.value),parseFloat(Ay.value));     
    var B:point = new point(parseFloat(Bx.value),parseFloat(By.value));
    var C:point = new point(parseFloat(Cx.value),parseFloat(Cy.value));
    var P:point = new point(parseFloat(Px.value),parseFloat(Py.value));    
    var pt:boolean = numcheck(A) && numcheck(B) && numcheck(C) && numcheck(P); //Is all 4 pts are valid
    var res:string;
    if (pt==true)
    {
        var Ar:number = Area(A,B,C);    //Area of triangle ABC
        if (Ar!=0)  //If triangle is not a straight line
        {
            var A1:number = Area(P,B,C);    //Area of triangle PBC
            var A2:number = Area(P,A,C);    //Area of triangle PAC
            var A3:number = Area(P,A,B);    //Area of triangle PAB   
            var sum:number = A1+A2+A3;
            if(Ar + 1 > sum && Ar-1 < sum)  // ie A1+A2+A3 must lie between Ar+1 and Ar-1. This is done because in case of decimal nos, Ar==Sum may be wrong.
            {
                res="YES the point P(" + P.x + "," +P.y +") lies inside the triangle ABC";
            }
            else
            {
                res="NO the point P(" + P.x + "," +P.y +") DOESNOT lie inside the triangle ABC";
            }
        }
        else
        {
            res = "The triangle is colinear ";
        }
    }
    else
    {
        res = "Please enter valid coordinates";
    }
    var para:HTMLParagraphElement = <HTMLParagraphElement>  document.getElementById("para");
    para.innerHTML="<h1>" +res+ "</h1>";
}
function Area(A:point,B:point,C:point) :number {
    var area:number = 0;
    var AB:number = Math.sqrt(Math.pow((A.x-B.x),2)+Math.pow((A.y-B.y),2))  //Length of side AB of triangle ABC
    var BC:number = Math.sqrt(Math.pow((B.x-C.x),2)+Math.pow((B.y-C.y),2))  //Length of side BC of triangle ABC
    var CA:number = Math.sqrt(Math.pow((A.x-C.x),2)+Math.pow((A.y-C.y),2))  //Length of side CA of triangle ABC
    var s:number = (AB+BC+CA)/2;                                            //SemiPerimeter s
    area = Math.sqrt(s*(s-AB)*(s-BC)*(s-CA));   //Area is calculated
    return area;
}

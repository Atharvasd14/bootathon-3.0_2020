var table = document.getElementById("table");
function solve() {
    var N = document.getElementById("T1");
    var num = parseFloat(N.value); //To handle float numbers
    while (table.rows.length != 0) {
        table.deleteRow(0);
    }
    if (!isNaN(num)) {
        var count = parseInt(N.value); //To make sure the for loop will make iterations= integer part of number
        var p = document.getElementById("p");
        p.innerHTML = "<h1>The table is:</h1>";
        for (var i = 1; i <= count; i = i + 1) {
            var row = table.insertRow();
            var cell = row.insertCell();
            cell.style.textAlign = "center";
            cell.bgColor = "red";
            cell.innerHTML = num.toString();
            var cell = row.insertCell();
            cell.style.textAlign = "center";
            cell.bgColor = "grey";
            cell.innerHTML = "*";
            var cell = row.insertCell();
            cell.style.textAlign = "center";
            cell.bgColor = "yellow";
            cell.innerHTML = i.toString();
            var cell = row.insertCell();
            cell.style.textAlign = "center";
            cell.bgColor = "gray";
            cell.innerHTML = "=";
            var cell = row.insertCell();
            cell.style.textAlign = "center";
            cell.bgColor = "red";
            cell.innerHTML = (num * i).toString();
        }
    }
    else {
        var p = document.getElementById("p");
        p.innerHTML = "<h1>PLEASE ENTER A VALID NUMBER</h1>";
    }
}
//# sourceMappingURL=mul_table.js.map